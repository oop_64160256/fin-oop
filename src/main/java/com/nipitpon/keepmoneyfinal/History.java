/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nipitpon.keepmoneyfinal;



/**
 *
 * @author nipit
 */
public class History {

    public History(double balance, double amount, String description, String incomeAndExpenese) {
        this.balance = balance;
        this.amount = amount;
        this.description = description;
        this.incomeAndExpenese = incomeAndExpenese;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIncomeAndExpenese() {
        return incomeAndExpenese;
    }

    public void setIncome(String income) {
        this.incomeAndExpenese = income;
    }

    @Override
    public String toString() {
        return incomeAndExpenese+" |"+description+" |"+amount+"THB |"+balance+"THB";
    }

   
    private double balance=0.00d;
    private double amount;
    private String description;
    private String incomeAndExpenese;
}
